package org.emids.InsuranceDomain;

import org.emids.InsuranceDomain.InsuranceQuote;

import static org.junit.Assert.*;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import org.junit.Test;

public class InsuranceTest {
	private InsuranceQuote servlet;
	private MockHttpServletRequest request;
	private MockHttpServletResponse response;

	@Before
	public void setUp() {
		servlet = new InsuranceQuote();
		request = new MockHttpServletRequest();
		response = new MockHttpServletResponse();
	}
	
	

	String hypertension = request.getParameter("hypertension") == null ? "false" : "true";
	String bloodpressure = request.getParameter("bloodpressure") == null ? "false" : "true";
	String bloodsugar = request.getParameter("bloodsugar") == null ? "false" : "true";
	String overweight = request.getParameter("overweight") == null ? "false" : "true";

	String smoking = request.getParameter("smoking") == null ? "false" : "true";
	String alcohol = request.getParameter("alcohol") == null ? "false" : "true";
	String dailyexercise = request.getParameter("dailyexercise") == null ? "false" : "true";
	String drugs = request.getParameter("drugs") == null ? "false" : "true";

	@Test
	public void testCalculateHttpServletRequestHttpServletResponse() throws ServletException, IOException {
		request.addParameter("gender", "male");  // male,female,other
		request.addParameter("age", "27");  //age
		request.addParameter("hypertension", "true");
		request.addParameter("bloodpressure", "true");
		request.addParameter("overweight", "true");
		request.addParameter("smoking", "true");
		request.addParameter("alcohol", "true");
		request.addParameter("dailyexercise", "true");
		request.addParameter("drugs", "true");  
		
		servlet.calculate(request, response);
		
		 assertEquals("text/html", response.getContentType());
		// Test result 

	}

}



